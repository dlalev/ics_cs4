#!/usr/bin/env python

import subprocess
import optparse


def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--interface", dest="interface", help="Interface to change its MAC address")
    parser.add_option("-m", "--mac", dest="macAddress", help="Add new MAC Address")
    # options contains the values of the input
    (options, arguments) = parser.parse_args()
    if not options.interface:
        parser.error("[-] Please specify an interface, use --help for more info")
    elif not options.macAddress:
        parser.error("[-] Please specify a MAC address, use --help for more info")
    return options


def change_mac(_interface, _macAddress):
    print("[+] Changing MAC address for " + _interface + " to " + _macAddress)

    # This way it cannot execute other commands
    subprocess.call(["ifconfig", _interface, "down"])
    subprocess.call(["ifconfig", _interface, "hw", "ether", _macAddress])
    subprocess.call(["ifconfig", _interface, "up"])


options = get_arguments()
#change_mac(options.interface, options.macAddress)

ifconfig_result = subprocess.check_output(["ifconfig", options.interface])
print(ifconfig_result)

